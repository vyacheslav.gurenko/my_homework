Student_1 = (74, "Failed")
Student_2 = (82, "Passed")
Student_3 = (97, "Passed")
Student_4 = (86, "Passed")
Student_5 = (67, "Passed")
Student_6 = (75, "Passed")
all_Students_1 = [Student_1, Student_2, Student_3, Student_4, Student_5, Student_6]

lower_past = 100
for Student in all_Students_1:
    grade_student = Student[1]
    grade = Student[0]
    if grade_student == "Passed":
       if grade < lower_past:
          lower_past = grade
print(f"Professor is not consistent {lower_past} - Failed")

student_1 = (84, "Passed")
student_2 = (78, "Passed")
student_3 = (65, "Failed")
student_4 = (90, "Passed")
student_5 = (72, "Failed")

all_students_1 = [student_1, student_2, student_3, student_4, student_5]

greatest_failed = 0
for student in all_students_1:
    grade_student = student[1]
    grade = student[0]
    if grade_student == "Failed":
        if grade > greatest_failed:
           greatest_failed = grade
print(f"Student with maximum {greatest_failed} - Failed")