user_input_1 = int(input("Input first digit: "))
user_input_2 = int(input("Input second digit: "))
if user_input_1 > user_input_2:
    print(f"First digit {user_input_1} greater than second digit {user_input_2}")
elif user_input_1 < user_input_2:
    print(f"First digit {user_input_1} less than second digit {user_input_2}")
else:
    print("Thank you for your choice")